package com.project.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="seat_allocation")
public class SeatAllocation {
	@Id
	private int seatAllotedId;
	private Employees employee;
	private Seats seat;
	private String designation;
	private String activeStatus;

	public int getSeatAllotedId() {
		return seatAllotedId;
	}

	public void setSeatAllotedId(int seatAllotedId) {
		this.seatAllotedId = seatAllotedId;
	}

	public Employees getEmployee() {
		return employee;
	}

	public void setEmployee(Employees employee) {
		this.employee = employee;
	}

	public Seats getSeat() {
		return seat;
	}

	public void setSeat(Seats seat) {
		this.seat = seat;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

}
