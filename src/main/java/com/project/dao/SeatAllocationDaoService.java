package com.project.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.project.model.Employees;
import com.project.model.SeatAllocation;
import com.project.model.Seats;


public interface SeatAllocationDaoService {

	List<Employees> getAllEmployees();

	Employees getEmployeeByCode(int employeeCode);

	Employees saveNewEmployees(Employees employee);

	Seats saveNewSeat(Seats seat);

	List<Seats> getAllSeats();

	Seats getSeatByCode(String seatCode);

	SeatAllocation saveAllotNewSeat(SeatAllocation seatAllocated);

	String deactivateAllotedSeat(String seatAllotedId);

	List<SeatAllocation> getAllAllotedSeats();

	String deactivateEmployee(String employeeId);

	String deactivateSeat(String seatCode);

	List<SeatAllocation> getSeatAllotedByEmployeeCode(String employeeCode);

	Seats getInactiveSeatByFloorandType(String seatType, int floor);

	Boolean checkIfSeatExists(int seatId);

	Boolean checkIfEmployeeExists(int employeeId);

	Boolean checkIfSeatAllotedExists(int seatAllotedId);


}
