package com.project.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import com.project.model.Employees;
import com.project.model.SeatAllocation;
import com.project.model.Seats;

@Repository
public class SeatAllocationDaoServiceImpl implements SeatAllocationDaoService {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Employees> getAllEmployees() {
		return mongoTemplate.findAll(Employees.class);
	}

	@Override
	public Employees getEmployeeByCode(int employeeCode) {
		Query query = new Query();
		query.addCriteria(Criteria.where("employeeCode").is(employeeCode).and("activeStatus").is("Y"));
		return mongoTemplate.findOne(query, Employees.class);
	}

	@Override
	public Employees saveNewEmployees(Employees employee) {
		mongoTemplate.save(employee);
		return employee;
	}

	@Override
	public Seats saveNewSeat(Seats seat) {
		mongoTemplate.save(seat);
		return seat;
	}

	@Override
	public List<Seats> getAllSeats() {
		return mongoTemplate.findAll(Seats.class);
	}

	@Override
	public Seats getSeatByCode(String seatCode) {
		Query query = new Query();
		query.addCriteria(Criteria.where("seatCode").is(seatCode).and("activeStatus").is("Y"));
		return mongoTemplate.findOne(query, Seats.class);
	}

	@Override
	public SeatAllocation saveAllotNewSeat(SeatAllocation seatAllocated) {
		mongoTemplate.save(seatAllocated);
		return seatAllocated;
	}

	@Override
	public String deactivateAllotedSeat(String seatAllotedId) {
		Query query = new Query();
		Update up = new Update();
		up.set("activeStatus", "N");
		query.addCriteria(Criteria.where("seatAllotedId").is(seatAllotedId));
		mongoTemplate.updateMulti(query, up, SeatAllocation.class);
		return "success";
	}

	@Override
	public List<SeatAllocation> getAllAllotedSeats() {
		return mongoTemplate.findAll(SeatAllocation.class);
	}

	@Override
	public String deactivateEmployee(String employeeCode) {
		Query query = new Query();
		Update up = new Update();
		up.set("activeStatus", "N");
		query.addCriteria(Criteria.where("employeeCode").is(employeeCode));
		mongoTemplate.updateMulti(query, up, Employees.class);
		return "success";
	}

	@Override
	public String deactivateSeat(String seatCode) {
		Query query = new Query();
		Update up = new Update();
		up.set("activeStatus", "N");
		query.addCriteria(Criteria.where("seatCode").is(seatCode));
		mongoTemplate.updateMulti(query, up, Seats.class);
		return "success";
	}

	@Override
	public List<SeatAllocation> getSeatAllotedByEmployeeCode(String employeeCode) {
		Query query = new Query();
		query.addCriteria(Criteria.where("employee").elemMatch(Criteria.where(employeeCode).is(employeeCode)));
		return mongoTemplate.find(query, SeatAllocation.class);
	}

	@Override
	public Seats getInactiveSeatByFloorandType(String seatType, int floor) {
		Query query = new Query();
		query.addCriteria(Criteria.where("seatType").is(seatType).and("floor").is(floor));
		return mongoTemplate.findOne(query, Seats.class);
	}

	@Override
	public Boolean checkIfSeatExists(int seatId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("seatId").is(seatId).and("activeStatus").is("Y"));
		return mongoTemplate.exists(query, Seats.class);
		}

	@Override
	public Boolean checkIfEmployeeExists(int employeeId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("employeeId").is(employeeId).and("activeStatus").is("Y"));
		return mongoTemplate.exists(query, Employees.class);
		}

	@Override
	public Boolean checkIfSeatAllotedExists(int seatAllotedId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("seatAllotedId").is(seatAllotedId).and("activeStatus").is("Y"));
		return mongoTemplate.exists(query, SeatAllocation.class);
		}

}
