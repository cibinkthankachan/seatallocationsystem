package com.project.controller;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.google.common.base.Predicate;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import static springfox.documentation.builders.PathSelectors.regex;
import static com.google.common.base.Predicates.or;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Bean
	public Docket postsApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("public-api")
				.apiInfo(apiInfo()).select().paths(postPaths()).build();
	}

	private Predicate<String> postPaths() {
		return or(regex("/create.*"), regex("/get.*"), regex("/employee.*"), regex("/deactivate.*"), regex("/update.*"), regex("/seat.*"), regex("/search.*"), regex("/allocate.*"), regex("/reallot.*"));
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder().title("Rest API")
				.description("Java API reference for SeatAllocationSystem")
				.termsOfServiceUrl("http://termsandService.com")
				.contact("150824@ust-global.com").license("Java License")
				.licenseUrl("150824@ust-global.com").version("1.0").build();
	}

}