package com.project.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.project.model.Employees;
import com.project.model.SeatAllocation;
import com.project.model.Seats;
import com.project.service.SeatAllocationService;

import ch.qos.logback.core.status.Status;

/*import com.project.service.SeatAllocationRepository;
*/
@RestController
@CrossOrigin(origins = "http://localhost:4200")
@ComponentScan(basePackages = {"com.project"})

public class SeatAllocationController {

	private final Logger LOG = LoggerFactory.getLogger(getClass());

	/*
	 * @Autowired private SeatAllocationRepository seatAllocationRepository;
	 */

	@Autowired
	private SeatAllocationService seatAllocationService;

	/**************** Add New Employees **************/
	@RequestMapping(value = "/createEmployee", method = RequestMethod.POST)
	public Employees addNewEmployees(@RequestBody Employees employee) {
		LOG.info("Saving Employee.");
		employee.setActiveStatus("Y");
		return seatAllocationService.saveNewEmployees(employee);
	}

	/**************** Get employee details by employeeId **************/
	@RequestMapping(value = "/employee/{employeeCode}", method = RequestMethod.GET)
	public Employees getEmployeeById(@PathVariable String employeeCode) {
		try {
			LOG.info("Getting Employee with ID: {}.", employeeCode);
			return seatAllocationService.getEmployeeByCode(Integer.parseInt(employeeCode));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**************** Deactivate alloted employees from seats **************/
	@RequestMapping(value = "/deactivateemployee/{employeeCode}", method = RequestMethod.GET)
	public String deactivateEmployee(@PathVariable String employeeCode) {
		LOG.info("Deactivate employee");
		return seatAllocationService.deactivateEmployee(employeeCode);
	}

	/**************** Update New Employees **************/
	@RequestMapping(value = "/updateEmployee", method = RequestMethod.POST)
	public Employees updateEmployee(@RequestBody Employees employee) {
		return seatAllocationService.saveNewEmployees(employee);
	}

	/**************** Get All Employees **************/
	@RequestMapping(value = "/getAllEmployees", method = RequestMethod.GET)
	public List<Employees> getAllEmployees() {
		LOG.info("Getting all Employees.");
		return seatAllocationService.getAllEmployees();
	}

	/**************** Create new seat **************/
	@RequestMapping(value = "/createSeat", method = RequestMethod.POST)
	public Seats addNewSeat(@RequestBody Seats seat) {
		LOG.info("Saving Seat.");
		return seatAllocationService.saveNewSeat(seat);
	}

	/****************
	 * Update with New Seat Details and deactivate existing
	 **************/
	@RequestMapping(value = "/updateSeatDetails", method = RequestMethod.POST)
	public Seats updateSeatDetails(@RequestBody Seats seat) {
		return seatAllocationService.saveNewSeat(seat);
	}

	/**************** Deactivate active seats **************/
	@RequestMapping(value = "/deactivateseat/{seatCode}", method = RequestMethod.GET)
	public String deactivateSeat(@PathVariable String seatCode) {
		LOG.info("Deactivate seat");
		return seatAllocationService.deactivateSeat(seatCode);
	}

	/**************** Get all Active and Inactive seats **************/
	@RequestMapping(value = "/getAllSeats", method = RequestMethod.GET)
	public List<Seats> getAllSeats() {
		LOG.info("Getting all Seats.");
		return seatAllocationService.getAllSeats();
	}

	/**************** Get seat by seatId **************/
	@RequestMapping(value = "/seat/{seatCode}", method = RequestMethod.GET)
	public Seats getSeat(@PathVariable String seatCode) {
		LOG.info("Getting Seat with Code: {}.", seatCode);
		return seatAllocationService.getSeatByCode(seatCode);
	}

	/**************** Allocate new employees to seat **************/
	@RequestMapping(value = "/allocateNewSeat", method = RequestMethod.POST)
	public SeatAllocation allocateNewSeat(@RequestBody SeatAllocation seatAllocated) {
		LOG.info("Allocating Seat for employee.");
		SeatAllocation seatAllocation = null;
		if (seatAllocated.getEmployee().getEmployeeId() == 0 && seatAllocated.getSeat().getSeatId() == 0) {
			LOG.info("Enter Valid employee/seat");
		} else {
			seatAllocation = seatAllocationService.saveAllotNewSeat(seatAllocated);
		}
		return seatAllocation;
	}

	/**************** Get Activeseat by employeeId **************/
	@RequestMapping(value = "/getSeatByEmployeeId/{employeeCode}", method = RequestMethod.GET)
	public List<SeatAllocation> getSeatByEmployeeCode(@PathVariable String employeeCode) {
		LOG.info("Getting Active Seat with ID: {}.", employeeCode);
		return seatAllocationService.getSeatAllotedByEmployeeCode(employeeCode);
	}

	/**************** Search for seats by seatType and floor **************/
	@RequestMapping(value = "/searchForInactiveSeats/{seatType}/{floor}", method = RequestMethod.GET)
	public Seats searchForInactiveSeats(@PathVariable String seatType, @PathVariable int floor) {
		LOG.info("Getting Inactive Seat with seatType and floor: {}." + seatType + "," + floor);
		return seatAllocationService.getInactiveSeatByFloorandType(seatType, floor);
	}

	/**************** Deactivate alloted employees from seats **************/
	@RequestMapping(value = "/deactivateAllocatedSeat/{allotedSeatId}", method = RequestMethod.GET)
	public String deactivateAllocatedSeat(@PathVariable String seatAllotedId) {
		LOG.info("Deactivate employee from alloted Seat");
		return seatAllocationService.deactivateAllotedSeat(seatAllotedId);
	}

	/**************** Reallot employees in new seats **************/
	@RequestMapping(value = "/reallotNewSeat", method = RequestMethod.POST)
	public SeatAllocation reallotNewSeat(@RequestBody SeatAllocation seatAllocated) {
		LOG.info("Allocating New Seat for employee.");
		SeatAllocation seatAllocation = null;
		seatAllocation = seatAllocationService.saveAllotNewSeat(seatAllocated);
		return seatAllocation;
	}

	/**************** Get all alloted seats with employees **************/
	@RequestMapping(value = "/getAllAllotedSeats", method = RequestMethod.GET)
	public List<SeatAllocation> getAllAllotedSeats() {
		LOG.info("Getting all Seats.");
		return seatAllocationService.getAllAllotedSeats();
	}
}