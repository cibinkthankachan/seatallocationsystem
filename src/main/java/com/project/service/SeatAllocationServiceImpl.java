package com.project.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.project.dao.SeatAllocationDaoService;
import com.project.model.Employees;
import com.project.model.SeatAllocation;
import com.project.model.Seats;

@Service
@Transactional(readOnly = true)
public class SeatAllocationServiceImpl implements SeatAllocationService {

	@Autowired
	private SeatAllocationDaoService seatAllocationDaoService;

	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@Override
	public List<Employees> getAllEmployees() {
		return seatAllocationDaoService.getAllEmployees();
	}

	@Override
	public Employees getEmployeeByCode(int employeeCode) {
		return seatAllocationDaoService.getEmployeeByCode(employeeCode);
	}

	@Transactional
	@Override
	public Employees saveNewEmployees(Employees employee) {
		Boolean exists = seatAllocationDaoService.checkIfEmployeeExists(employee.getEmployeeId());
		if (employee.getEmployeeId() != 0 && exists == true) {
			RestTemplate restTemplate = new RestTemplate();
			employee.setEmployeeId(0);
			// Deactivating Employees
			String url = "http://localhost:8102/seatallocationsystem/deactivateemployee/" + employee.getEmployeeId();
			String message = restTemplate.getForObject(url, String.class);
			if (message == "success") {
				LOG.info("Updating Employee.");
				employee.setActiveStatus("Y");
				return seatAllocationDaoService.saveNewEmployees(employee);
			} else {
				LOG.info("Issue In Update");
				return null;
			}
		} else {
			return seatAllocationDaoService.saveNewEmployees(employee);
		}
	}

	@Override
	@Transactional
	public Seats saveNewSeat(Seats seat) {
		Boolean exists = seatAllocationDaoService.checkIfSeatExists(seat.getSeatId());
		if (seat.getSeatId() != 0 && exists == true) {
			RestTemplate restTemplate = new RestTemplate();
			seat.setSeatId(0);
			// Deactivating Employees
			String url = "http://localhost:8102/seatallocationsystem/deactivateseat/" + seat.getSeatCode();
			String message = restTemplate.getForObject(url, String.class);
			if (message == "success") {
				LOG.info("Updating Seat.");
				seat.setActiveStatus("Y");
				return seatAllocationDaoService.saveNewSeat(seat);
			} else {
				LOG.info("Issue In Update");
				return null;

			}
		} else {
			return seatAllocationDaoService.saveNewSeat(seat);
		}
	}

	@Override
	public List<Seats> getAllSeats() {
		return seatAllocationDaoService.getAllSeats();
	}

	@Override
	public Seats getSeatByCode(String seatCode) {
		return seatAllocationDaoService.getSeatByCode(seatCode);
	}

	@Override
	@Transactional
	public SeatAllocation saveAllotNewSeat(SeatAllocation seatAllocated) {
		SeatAllocation seatAllocation = null;
		Boolean exists = seatAllocationDaoService.checkIfSeatAllotedExists(seatAllocated.getSeatAllotedId());
		if (seatAllocated.getSeatAllotedId() != 0 && exists == true) {

			RestTemplate restTemplate = new RestTemplate();
			seatAllocated.setSeatAllotedId(0);
			if (seatAllocated.getEmployee().getEmployeeId() != 0 && seatAllocated.getSeat().getSeatId() != 0) {
				LOG.info("Enter Valid employee/seat");
			} else {
				String url = "http://localhost:8102/seatallocationsystem/deactivateAllocatedSeat/"
						+ seatAllocated.getSeatAllotedId();
				String message = restTemplate.getForObject(url, String.class);
				if (message == "success") {
					seatAllocated.setActiveStatus("Y");
					seatAllocation = seatAllocationDaoService.saveAllotNewSeat(seatAllocated);
				}
			}
		} else {
			seatAllocation = seatAllocationDaoService.saveAllotNewSeat(seatAllocated);
		}
		return seatAllocation;
	}

	@Override
	@Transactional
	public String deactivateAllotedSeat(String seatAllotedId) {
		return seatAllocationDaoService.deactivateAllotedSeat(seatAllotedId);
	}

	@Override
	public List<SeatAllocation> getAllAllotedSeats() {
		return seatAllocationDaoService.getAllAllotedSeats();
	}

	@Override
	@Transactional
	public String deactivateEmployee(String employeeId) {
		return seatAllocationDaoService.deactivateEmployee(employeeId);
	}

	@Override
	@Transactional
	public String deactivateSeat(String seatCode) {
		return seatAllocationDaoService.deactivateSeat(seatCode);

	}

	@Override
	public List<SeatAllocation> getSeatAllotedByEmployeeCode(String employeeCode) {
		return seatAllocationDaoService.getSeatAllotedByEmployeeCode(employeeCode);
	}

	@Override
	public Seats getInactiveSeatByFloorandType(String seatType, int floor) {
		return seatAllocationDaoService.getInactiveSeatByFloorandType(seatType, floor);
	}

	public SeatAllocationDaoService getSeatAllocationDaoService() {
		return seatAllocationDaoService;
	}

	public void setSeatAllocationDaoService(SeatAllocationDaoService seatAllocationDaoService) {
		this.seatAllocationDaoService = seatAllocationDaoService;
	}

}
